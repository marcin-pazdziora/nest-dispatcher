module.exports = {
  name: "Go2Automation API",
  module: "commonjs",
  target: "es6",
  mode: "modules",
  out: "docs",
  theme: "default",
  ignoreCompilerErrors: true,
  preserveConstEnums: true,
  exclude: [
    __dirname + "/src/common/**/*",
    __dirname + "/src/main*.ts",
    __dirname + "/src/App*.ts",
    __dirname + "/src/**/*.spec.ts",
    __dirname + "/src/**/*Service.ts",
    __dirname + "/src/**/*Entity.ts",
    __dirname + "/src/**/*.module.ts",
    __dirname + "/src/migrations/**/*",
    __dirname + "/src/**/*Algorithm.ts"
  ],
  excludeExternals: true,
  excludePrivate: true,
  excludeProtected: true,
  stripInternal: false
};
