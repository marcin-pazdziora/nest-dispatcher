import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from "@nestjs/common";
import { ConfigService } from "./common/services/config/ConfigService";
import { AppModule } from './app.module';
import * as dotenv from "dotenv";
import * as helmet from "helmet";
import * as rateLimit from "express-rate-limit";

if (process.env.NODE_ENV !== "production") {
  dotenv.load();
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = app.get("ConfigService") as ConfigService;

  app.use(helmet());
  app.use(
    new rateLimit({
      windowMs: config.get("app.security.rateLimit.in"),
      max: config.get("app.security.rateLimit.times")
    })
  );
  app.useGlobalPipes(
    new ValidationPipe({
      disableErrorMessages: config.get("app.env") === "production",
      transform: true,
      forbidNonWhitelisted: true
    })
  );
  app.useGlobalInterceptors(new LoggingInterceptor());
  // app.enableCors();

  await app.listen(config.get("app.port"));
}

bootstrap();
