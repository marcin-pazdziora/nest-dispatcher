import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  Logger
} from "@nestjs/common";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { Response } from "express-serve-static-core";

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  intercept(
    context: ExecutionContext,
    call$: Observable<any>
  ): Observable<any> {
    const req = context.switchToHttp().getRequest();
    const res = context.switchToHttp().getResponse() as Response;
    Logger.log(`${req.method} ${req.originalUrl} - ${res.statusCode}`, "REQ");
    return call$.pipe();
  }
}
