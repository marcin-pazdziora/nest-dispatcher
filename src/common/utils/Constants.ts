export const C = {
  NOT_FOUND_IN_ARRAY: -1,
  NOT_FOUND_IN_STRING: -1,
  SQL_RESULT_PREFIX_LENGTH: 7, // for slicing off 'UPDATE ' part of result return string
  SQL_RESULT_FIRST_ROW: 0 // index of first row in the results
};

export const HTTP_STATUS = {
  FAILED_DEPENDENCY: 424,
  UNAVAILABLE_LEGAL_REASONS: 451
}
