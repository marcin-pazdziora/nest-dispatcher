import * as _ from "lodash";
import * as fs from "fs";
import { sync as glob } from "glob";

export function loadSync(
  filename: string,
  mode?: FileModes | FileModes[]
): string {
  if (!isAccessible(filename, mode)) {
    throw new Error(`File: ${filename} is unreachable`);
  }
  return fs.readFileSync(filename, "utf8");
}

export function isAccessible(
  fileName: string,
  mode: FileModes | FileModes[] = ["read"]
): boolean {
  if (typeof mode === "string") {
    mode = [mode];
  }

  try {
    fs.accessSync(
      fileName,
      mode.map(cv => FileMode[cv]).reduce((pv, cv) => pv | cv) // tslint:disable-line no-bitwise
    );
  } catch (err) {
    return false;
  }
  return true;
}

export function getJsonMap<V, K = string>(filename: string): Map<K, V> {
  const parsedJson = JSON.parse(loadSync(filename));

  if (
    !(
      _.isArray(parsedJson) &&
      parsedJson.every(val => _.isArray(val) && val.length === 2)
    )
  ) {
    throw new Error(`File "${filename}" doesn't contain a JSON-encoded map`);
  }

  return new Map<K, V>(JSON.parse(loadSync(filename)));
}
export function getJsonObject(filename: string): { [key: string]: any } {
  return JSON.parse(loadSync(filename));
}

export function globImport<T>(
  pattern: string,
  fn: (imported: T, filename: string) => void = i => i
): Promise<void>[] {
  const promises: Promise<void>[] = [];
  for (const filename of glob(pattern)) {
    promises.push(
      import(filename).then((imported: T) => fn(imported, filename))
    );
  }

  return promises;
}

export type FileModes = "exists" | "read" | "write" | "execute";

enum FileMode {
  exists = fs.constants.F_OK,
  read = fs.constants.R_OK,
  write = fs.constants.W_OK,
  execute = fs.constants.X_OK
}
