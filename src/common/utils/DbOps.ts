import { QueryFailedError, InsertResult, UpdateResult } from "typeorm";
import { HttpException, HttpStatus } from "@nestjs/common";
import { has as _has } from "lodash";

export function catchErr<T>(err: T, options: DbErrorOptions = {}): T {
  if (options.duplicate) {
    duplicateErr(err, options.duplicate);
  }

  if (err instanceof QueryFailedError) {
    console.log((err as any).query); // tslint:disable-line

    throw new HttpException(
      (options.dbErrorMsg || "DB error: ") + err.message,
      500
    );
  }

  return err;
}

function duplicateErr(
  err: any,
  options: NonNullable<DbErrorOptions["duplicate"]>
): void {
  console.log("ERR CODE: ", err.code);

  let { msg, httpStatus } = options; // tslint:disable-line
  if (typeof httpStatus === "undefined") {
    httpStatus = HttpStatus.FORBIDDEN;
  }
  if (err.message && err.message.startsWith("duplicate key value")) {
    throw new HttpException(msg, httpStatus);
  }
}

export function emptySelectResult(
  subject: any,
  msg: string,
  httpStatus: HttpStatus = HttpStatus.NOT_FOUND
) {
  if (typeof subject === "undefined") {
    throw new HttpException(msg, httpStatus);
  }
}

export function getInsertId(
  result: InsertResult,
  msg: string = "Failed to insert an entity",
  httpStatus: HttpStatus = HttpStatus.INTERNAL_SERVER_ERROR
): number {
  if (typeof result.identifiers[0].id === "undefined") {
    throw new HttpException(msg, httpStatus);
  }
  return result.identifiers[0].id;
}

export function getUpdateId(
  result: UpdateResult,
  msg: string = "Not found",
  httpStatus: HttpStatus = HttpStatus.NOT_FOUND
) {
  if (!_has(result, "raw[0].id")) {
    throw new HttpException(msg, httpStatus);
  }
}

interface DbErrorOptions {
  dbErrorMsg?: string;
  duplicate?: DbExceptionOption;
  emptyQueryResult?: DbExceptionOption & { subject: any };
}

interface DbExceptionOption {
  msg: string;
  httpStatus?: HttpStatus;
}
