import * as _ from "lodash";

export function getTimeSec(time?: number | Date): number {
  if (typeof time === "undefined") {
    time = new Date().getTime();
  }
  if (typeof time !== "number") {
    time = time.getTime();
  }
  return Math.round(time / 1000);
}

export function omitUnset(obj: {
  [key: string]: any;
}): Partial<{ [key: string]: any }> {
  return _.pickBy(obj, (val: any) => typeof val !== "undefined");
}

export type Omit<T, U> = Pick<T, Exclude<keyof T, U>>;