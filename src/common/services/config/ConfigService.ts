import { Injectable } from "@nestjs/common";
import * as _ from "lodash";
import * as FileOps from "../../utils/FileOps";

@Injectable()
export class ConfigService {
  private conf: {} = {};

  async mergeDir(
    dirname: string,
    recursive: boolean = false,
    except: string[] = []
  ): Promise<this> {
    await FileOps.globImport(
      `${dirname}/${recursive ? "**/" : ""}*.conf.json`,
      (imported: {}, filename: string) => {
        if (!except.includes(filename)) this.merge(imported);
      }
    );
    return this;
  }

  mergeFile(filename: string): this {
    this.merge(FileOps.getJsonObject(filename));
    return this;
  }

  mergeEnv(specFilename: string): this {
    const spec = FileOps.getJsonObject(specFilename);
    let envName: string;
    for (const env of spec.vars) {
      envName = (env.var as string).startsWith("_")
        ? spec.prefix + env.var
        : env.var;
      _.set(this.conf, env.conf, process.env[envName] || env.default);
    }
    return this;
  }

  merge(conf: {}): this {
    _.merge(this.conf, conf);
    return this;
  }

  has(path: string): boolean {
    return _.has(this.conf, path);
  }

  get<T>(path: string, defaultVal?: T): T {
    if (!this.has(path)) {
      if (typeof defaultVal !== "undefined") {
        return defaultVal;
      }
      throw new Error(`No such path: "${path}"`);
    }

    return _.get(this.conf, path);
  }
}
