import { KvStoreInterface } from "./KvStoreInterface";
import { ConfigService } from "../config/ConfigService";

export abstract class AbstractKvStore implements KvStoreInterface {
  constructor(
    protected readonly storeName: string,
    protected readonly config: ConfigService
  ) {}
  abstract has(key: string): boolean;
  abstract get(key: string): any;
  abstract set(key: string, val: any, expires?: number): void;
  abstract del(key: string): void;
  abstract save(): void;
  abstract cleanup(): void;
}
