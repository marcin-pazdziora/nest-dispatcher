import { Injectable } from "@nestjs/common";
import * as fs from "fs";
import * as FileOps from "../../utils/FileOps";
import * as _ from "lodash";
import { ConfigService } from "../config/ConfigService";
import { AbstractKvStore } from "./AbstractKvStore";
import { getTimeSec } from "../../utils/Util";
import { KvStoreFactoryInterface } from "./KvStoreInterface";

const C = {
  NEVER_EXPIRES: 0
};

@Injectable()
export default class MemoryStoreFactory implements KvStoreFactoryInterface {
  constructor(protected readonly config: ConfigService) {}

  create(storeName: string) {
    return new MemoryStore(storeName, this.config);
  }
}

class MemoryStore extends AbstractKvStore {
  private readonly store: Map<string, { val: any; expires: number }>;
  private readonly saveFile: string;

  constructor(storeName: string, config: ConfigService) {
    super(storeName, config);
    this.saveFile = `${this.config.get(
      "store.dumpDir"
    )}/${storeName}.dump.json`;
    this.store = FileOps.isAccessible(this.saveFile, ["read", "write"])
      ? FileOps.getJsonMap(this.saveFile)
      : new Map<string, any>();
  }

  has(key: string): boolean {
    return typeof this.get(key) !== "undefined";
  }

  get(key: string): any {
    const data = this.store.get(key);
    if (typeof data === "undefined") {
      return undefined;
    }
    if (this.isValid(data.expires)) {
      return data.val;
    }

    this.del(key);
    return undefined;
  }

  set(key: string, val: any, expires: number = C.NEVER_EXPIRES): void {
    this.store.set(key, { val, expires });
  }

  del(key: string): void {
    this.store.delete(key);
  }

  save(): void {
    this.cleanup();
    fs.writeFileSync(this.saveFile, JSON.stringify([...this.store]));
  }

  cleanup(): void {
    const currentTime = getTimeSec();
    for (const [key, data] of this.store) {
      if (!this.isValid(data.expires, currentTime)) {
        this.store.delete(key);
      }
    }
  }

  private isValid(expiration: number, time?: number) {
    return (
      expiration === C.NEVER_EXPIRES || expiration > (time || getTimeSec())
    );
  }
}
