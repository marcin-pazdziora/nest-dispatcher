import { Test, TestingModule } from "@nestjs/testing";
import MemoryStore from "./MemoryStore";

describe("MemoryService", () => {
  let service: MemoryStore;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MemoryStore]
    }).compile();
    service = module.get<MemoryStore>(MemoryStore);
  });
  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
