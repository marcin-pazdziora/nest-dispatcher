import { ConfigService } from "../config/ConfigService";

export interface KvStoreFactoryCtor {
  new (config: ConfigService): KvStoreInterface;
}

export interface KvStoreFactoryInterface {
  create(storeName: string): KvStoreInterface;
}

export interface KvStoreInterface {
  has(key: string): boolean;
  get(key: string): any;
  set(key: string, val: any, expires?: number): void;
  del(key: string): void;
  save(): void;
  cleanup(): void;
}
