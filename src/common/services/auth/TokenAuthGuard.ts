import { AuthGuard } from "@nestjs/passport";
import { ExecutionContext } from "@nestjs/common";

export class TokenAuthGuard extends AuthGuard("jwt") {
  canActivate(context: ExecutionContext) {
    // tslint:disable-next-line:no-console
    console.log(context);
    return super.canActivate(context);
  }
}
