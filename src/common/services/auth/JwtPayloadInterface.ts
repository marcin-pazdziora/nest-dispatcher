import { TOKEN } from "./tokens/TokenHandlerInterface";
import { VerificationStage } from "../../../person/entities/PersonEntity";

export interface JwtPayload {
  type: TOKEN;
  issuedAt: number;
}

export interface LoginJwtPayload extends JwtPayload {
  userId: number;
  salutation: string;
  firstName: string;
  lastName: string;
  email: string;
  contactEmail: string;
  companyId: number;
  tncAccepted: number;
  stage: VerificationStage;
  languageCode: string;
}

export interface EmailJwtPayload extends JwtPayload {
  email: string;
}
