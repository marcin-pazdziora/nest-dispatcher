import {
  Injectable,
  Inject,
  InternalServerErrorException
} from "@nestjs/common";
import { KvStoreFactoryInterface } from "../kvStore/KvStoreInterface";
import {
  TOKEN,
  TokenHandlerInterface,
  TokenHandlerCtor
} from "./tokens/TokenHandlerInterface";
import { ConfigService } from "../config/ConfigService";
import { JwtService } from "@nestjs/jwt";
import { PersonService } from "../../../person/PersonService";
import { globImport } from "../../utils/FileOps";
import { basename } from "path";

@Injectable()
export class AuthService {
  private readonly handlerTypes: Map<string, TokenHandlerCtor> = new Map();
  private readonly handlers: Map<string, TokenHandlerInterface>;

  constructor(
    protected readonly config: ConfigService,
    protected readonly jwt: JwtService,
    protected readonly person: PersonService,
    @Inject("KvStoreService")
    protected readonly kvStore: KvStoreFactoryInterface
  ) {
    const promises = globImport(
      __dirname + "/tokens/!(Abstract)*TokenHandler.?s",
      (imported: any, filename: string) => {
        this.handlerTypes.set(
          basename(filename).slice(0, -15),
          imported.default
        );
      }
    );

    this.handlers = new Map();
    Promise.all(promises).then(() => {
      this.handlers.set(
        TOKEN.AcceptTerms,
        this.createHandler("SingleUse", TOKEN.AcceptTerms)
      );
      this.handlers.set(
        TOKEN.DeleteAccount,
        this.createHandler("SingleUse", TOKEN.DeleteAccount)
      );
      this.handlers.set(
        TOKEN.EndPremium,
        this.createHandler("SingleUse", TOKEN.EndPremium)
      );
      this.handlers.set(
        TOKEN.ResetPassword,
        this.createHandler("SingleUse", TOKEN.ResetPassword)
      );
      this.handlers.set(
        TOKEN.Session,
        this.createHandler("Renewable", TOKEN.Session)
      );
    });
  }

  private createHandler(type: string, name: TOKEN) {
    const tokenHandler = this.handlerTypes.get(type);
    if (typeof tokenHandler === "undefined") {
      throw new InternalServerErrorException(`No such token type: "${type}"`);
    }
    return new tokenHandler(
      this.config,
      this.jwt,
      this.person,
      this.kvStore.create(name),
      this,
      name
    );
  }

  get(handler: TOKEN): TokenHandlerInterface {
    const tokenHandler = this.handlers.get(handler);
    if (typeof tokenHandler === "undefined") {
      throw new InternalServerErrorException(
        `No such token handler: "${handler}"`
      );
    }
    return tokenHandler;
  }
}
