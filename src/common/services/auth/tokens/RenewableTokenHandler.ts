import { JwtTokenInterface, TOKEN } from "./TokenHandlerInterface";
import { LoginJwtPayload } from "../JwtPayloadInterface";
import { AbstractTokenHandler } from "./AbstractTokenHandler";
import { HttpException, HttpStatus } from "@nestjs/common";
import { getTimeSec } from "../../../utils/Util";
import { PersonEntity } from "../../../../person/entities/PersonEntity";
import { UnavailableLegalReasonsException } from "../../../utils/UnavailableLegalReasonsException";

export default class RenewableTokenHandler extends AbstractTokenHandler {
  async createToken(payloadData: LoginJwtPayload): Promise<JwtTokenInterface> {
    return this.tokenCreation(
      this.type,
      payloadData.userId.toString(),
      payloadData
    );
  }

  async validateToken(payload: LoginJwtPayload): Promise<PersonEntity> {
    if (
      payload.issuedAt + this.config.get<number>("store.ttl") < getTimeSec() ||
      !this.store.has(payload.userId.toString())
    ) {
      this.destroyToken(payload.userId);
      throw new HttpException("Session time exceeded", HttpStatus.UNAUTHORIZED);
    }

    if (
      payload.tncAccepted !== this.config.get("app.termsAndConditionsVersion")
    ) {
      this.destroyToken(payload.userId);
      const token = await this.auth
        .get(TOKEN.AcceptTerms)
        .createToken({ email: payload.email });
      throw new UnavailableLegalReasonsException(
        "User have not accepted the newest Terms&Conditions version",
        token
      );
    }
    const person = await this.person.fetchPerson(payload.userId);
    person.auth.tokenType = payload.type;
    return person;
  }
}
