import { JwtTokenInterface } from "./TokenHandlerInterface";
import { AbstractTokenHandler } from "./AbstractTokenHandler";
import { HttpException, HttpStatus } from "@nestjs/common";
import { EmailJwtPayload, JwtPayload } from "../JwtPayloadInterface";
import { PersonEntity } from "../../../../person/entities/PersonEntity";

export default class SingleUseTokenHandler extends AbstractTokenHandler {
  createToken(payloadData: EmailJwtPayload): Promise<JwtTokenInterface> {
    return this.tokenCreation(this.type, payloadData.email, payloadData);
  }

  async validateToken(payload: EmailJwtPayload): Promise<PersonEntity> {
    if (!this.store.has(payload.email)) {
      throw new HttpException("Invalid token", HttpStatus.UNAUTHORIZED);
    }
    this.destroyToken(payload.email);
    const person = await this.person.fetchPerson(payload.email);
    person.auth.tokenType = payload.type;
    return person;
  }
}
