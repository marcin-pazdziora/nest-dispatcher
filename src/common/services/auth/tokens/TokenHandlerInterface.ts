import { ObjectLiteral } from "typeorm";
import { ConfigService } from "../../config/ConfigService";
import { JwtService } from "@nestjs/jwt";
import { PersonService } from "../../../../person/PersonService";
import { KvStoreInterface } from "../../kvStore/KvStoreInterface";
import { AuthService } from "../AuthService";

export interface TokenHandlerCtor {
  new (
    config: ConfigService,
    jwt: JwtService,
    person: PersonService,
    store: KvStoreInterface,
    auth: AuthService,
    name: TOKEN
  ): TokenHandlerInterface;
}

export interface TokenHandlerInterface {
  createToken(payloadData: ObjectLiteral): Promise<JwtTokenInterface>;
  validateToken(payload: ObjectLiteral): Promise<any>;
  destroyToken(id: string | number): Promise<void>;
}

export interface JwtTokenInterface {
  type: TOKEN;
  expiresIn: number;
  accessToken: string;
}

export enum TOKEN {
  AcceptTerms = "AcceptTerms",
  DeleteAccount = "DeleteAccount",
  EndPremium = "EndPremium",
  ResetPassword = "ResetPassword",
  Session = "Session"
}
