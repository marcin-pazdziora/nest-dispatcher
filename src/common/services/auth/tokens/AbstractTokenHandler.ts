import { JwtTokenInterface, TOKEN } from "./TokenHandlerInterface";
import { ConfigService } from "../../config/ConfigService";
import { JwtService } from "@nestjs/jwt";
import { KvStoreInterface } from "../../kvStore/KvStoreInterface";
import { PersonService } from "../../../../person/PersonService";
import { ObjectLiteral } from "typeorm";
import { getTimeSec } from "../../../utils/Util";
import { AuthService } from "../AuthService";

export abstract class AbstractTokenHandler {
  constructor(
    protected readonly config: ConfigService,
    protected readonly jwt: JwtService,
    protected readonly person: PersonService,
    protected store: KvStoreInterface,
    protected readonly auth: AuthService,
    protected readonly type: TOKEN
  ) {}

  abstract async createToken(
    payloadData: ObjectLiteral
  ): Promise<JwtTokenInterface>;
  abstract async validateToken(payload: ObjectLiteral): Promise<any>;

  async destroyToken(id: string | number): Promise<void> {
    if (typeof id === "number") {
      id = id.toString();
    }
    await this.store.del(id);
  }

  protected async tokenCreation<T>(
    type: TOKEN,
    key: string,
    payload: T
  ): Promise<JwtTokenInterface> {
    const issuedAt = getTimeSec();
    payload = Object.assign(payload, { type, issuedAt });
    const accessToken = await this.jwt.sign(payload);
    const ttl: number = this.config.get<number>("store.ttl");

    this.store.set(key, payload, issuedAt + ttl);
    return {
      type,
      expiresIn: ttl,
      accessToken
    };
  }
}
