import { createParamDecorator, ReflectMetadata } from "@nestjs/common";

export const User = createParamDecorator((data, req) => req.user);
export const Roles = (...roles: string[]) => ReflectMetadata("roles", roles);
