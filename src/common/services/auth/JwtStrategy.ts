import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { AuthService } from "./AuthService";
import {
  LoginJwtPayload,
  EmailJwtPayload,
  JwtPayload
} from "./JwtPayloadInterface";
import { ConfigService } from "../config/ConfigService";
import { TOKEN } from "./tokens/TokenHandlerInterface";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly auth: AuthService, config: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: config.get("app.security.jwtSecretKey")
    });
  }

  async validate(payload: Payload): Promise<any> {
    return this.auth.get(payload.type).validateToken(payload);
  }
}

type Payload = JwtPayload & (LoginJwtPayload | EmailJwtPayload);
