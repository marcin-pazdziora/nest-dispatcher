import { Test, TestingModule } from "@nestjs/testing";
import { AuthService } from "./AuthService";

describe("AuthService", () => {
  let service: AuthService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AuthService]
    }).compile();
    service = module.get<AuthService>(AuthService);
  });
  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
