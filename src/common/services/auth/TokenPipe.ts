import {
  PipeTransform,
  ArgumentMetadata,
  ForbiddenException,
  Injectable
} from "@nestjs/common";
import { PersonEntity } from "../../../person/entities/PersonEntity";

@Injectable()
export class TokenPipe implements PipeTransform {
  transform(user: PersonEntity, metadata: ArgumentMetadata): PersonEntity {
    const tokenType = user.auth.tokenType;
    if (tokenType !== metadata.data) {
      throw new ForbiddenException(
        `'${tokenType}' token passed. Expected '${metadata.data}' type token`
      );
    }
    return user;
  }
}
