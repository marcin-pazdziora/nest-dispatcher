import { Injectable, InternalServerErrorException } from "@nestjs/common";
import { createCipher, randomBytes, pbkdf2, createDecipher } from "crypto";

@Injectable()
export class CryptoService {
  createNumString(length: number): string {
    return [...new Array(length)]
      .map(() => Math.floor(Math.random() * 10))
      .join("");
  }

  createSalt(length: number): string {
    return randomBytes(length).toString();
  }

  hash(val: any, salt?: string): Promise<string> {
    if (typeof salt === "undefined") {
      salt = new Date().getTime().toString();
    }
    const text = this.toString(val);

    return new Promise<string>((resolved, rejected) => {
      pbkdf2(text, salt as string, 10000, 64, "sha512", (err, hash) => {
        if (err) {
          return rejected("Error hashing password");
        }
        return resolved(hash.toString("base64"));
      });
    });
  }

  cipher(val: any, salt: string): string {
    const text = this.toString(val);

    const cipher = createCipher("aes192", salt);
    let encrypted = cipher.update(text, "utf8", "hex");
    return (encrypted += cipher.final("hex"));
  }

  decipher(encrypted: string, salt: string): string {
    const decipher = createDecipher("aes192", salt);
    let decrypted = decipher.update(encrypted, "hex", "utf8");
    return (decrypted += decipher.final("utf8"));
  }

  private toString(val: any): string {
    switch (typeof val) {
      case "string":
        return val;
      case "number":
        return val.toString();
      case "boolean":
        return val ? "1" : "0";
      case "undefined":
        return "";
      default:
        if (val === null) {
          return "";
        }
        throw new InternalServerErrorException("Value cannot be hashed");
    }
  }
}
