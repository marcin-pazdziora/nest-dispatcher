import { Module, Global } from '@nestjs/common';
import { ConfigService } from "./services/config/ConfigService";
import { KvStoreFactoryCtor } from "./services/kvStore/KvStoreInterface";
import * as _ from "lodash";
import { PassportModule } from "@nestjs/passport";
import { JwtModule } from "@nestjs/jwt";
import { AuthService } from "./services/auth/AuthService";
import { CryptoService } from "./services/security/CryptoService";
import { JwtStrategy } from "./services/auth/JwtStrategy";

// ConfigService cannot be configured in the module's constructor
// because decorators are expanded on module load,
// well before module is instantiated
const Config = {
    provide: "ConfigService",
    useFactory: async () => {
        const config = new ConfigService();
        await config.mergeDir(__dirname + "/../../conf");
        config.mergeEnv(__dirname + "/../../conf/env.spec.json");
        return config;
    }
};

// Here, the Strategy pattern is used to instantiate KvStoreService
// Store class is chosen based on the config value.
// ATM, choices are: MemoryStore and RedisStore
const KvStore = {
    provide: "KvStoreService",
    useFactory: async (config: ConfigService) => {
        const store = _.upperFirst(config.get("store.type")) + "Store";
        return await import(__dirname + "/services/kvStore/" + store).then(
            (imported: { default: KvStoreFactoryCtor }) => {
                return new imported.default(config);
            }
        );
    },
    inject: [ConfigService]
};

@Global()
@Module({
    imports: [
        PassportModule.register({ defaultStrategy: "jwt" }),
        JwtModule.registerAsync({
            inject: [ConfigService],
            useFactory: (config: ConfigService) => ({
                secretOrPrivateKey: config.get("app.security.jwtSecretKey"),
                signOptions: {
                    expiresIn: config.get("store.ttl")
                }
            })
        })
    ],
    providers: [
        AuthService,
        Config,
        CryptoService,
        JwtStrategy,
        KvStore
    ],
    exports: [
        AuthService,
        CryptoService,
        "ConfigService",
        "KvStoreService"
    ]
})
export class CommonModule { }
