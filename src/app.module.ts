import { Module } from '@nestjs/common';
import { AppController } from './AppController';
import { AppService } from './AppService';
import { DispatcherModule } from './dispatcher/dispatcher.module';
import { CommonModule } from "./common/common.module";

@Module({
  imports: [CommonModule, DispatcherModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
