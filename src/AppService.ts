import { Injectable } from '@nestjs/common';
import { ConfigService } from "./common/services/config/ConfigService";

@Injectable()
export class AppService {
  constructor(
    private readonly configService: ConfigService
  ) { }

  root(): string {
    const result = `<h1><b>${this.configService.get('app.siteName')} API</b> v${this.configService.get('app.currentRelease')}</h1>`;
    return result;
  }
}
